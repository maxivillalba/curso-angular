import { Injectable, Inject, forwardRef } from '@angular/core';
import { DestinoViaje } from './destino-viaje.model';
import {
  HttpRequest,
  HttpHeaders,
  HttpClient,
  HttpEvent,
  HttpResponse,
} from '@angular/common/http';

@Injectable()
export class DestinosApiClient {
  destinos: DestinoViaje[] = [];

  constructor() {}

  add(d: DestinoViaje) {
    const headers: HttpHeaders = new HttpHeaders({
      'X-API-TOKEN': 'token-seguridad',
    });
    const req = new HttpRequest(
      'POST',
      this.config.apiEndpoint + '/my',
      { nuevo: d.nombre },
      { headers: headers }
    );
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
      if (data.status === 200) {
        this.store.dispatch(new NuevoDestinoAction(d));
        const myDb = db;
        myDb.destinos.add(d);
        console.log('todos los destinos de la db!');
        myDb.destinos.toArray().then((destinos) => console.log(destinos));
      }
    });
  }

  getAll(): DestinoViaje[] {
    return this.destinos;
  }
}
